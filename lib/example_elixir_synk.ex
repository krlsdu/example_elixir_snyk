defmodule ExampleElixirSynk do
  @moduledoc """
  Documentation for `ExampleElixirSynk`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> ExampleElixirSynk.hello()
      :world

  """
  def hello do
    :world
  end
end
